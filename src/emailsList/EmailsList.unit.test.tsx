import * as React from 'react'
import { shallow } from 'enzyme'
import { Actions, EmailsList, EmailsListItem } from 'emailsList'
import * as Styles from './styles'
import { emailMock } from 'dev'

const mockDispatch = jest.fn()
const mockSelector = jest.fn()

jest.mock('react-redux', () => ({
    useSelector: () => mockSelector(),
    useDispatch: () => mockDispatch
}))

describe('EmailsList tests', () => {

    beforeEach(() => {
        mockSelector.mockReturnValueOnce([
            { ...emailMock, _id: '0' },
            { ...emailMock, _id: '1' },
            { ...emailMock, _id: '2' }
        ]).mockReturnValueOnce({ ...emailMock, _id: '0' })
    })

    it('should properly render EmailsList component with 3 items', () => {
        const wrapper = shallow(<EmailsList/>)
        expect(wrapper.find(Styles.Wrapper)).toHaveLength(1)
        expect(wrapper.find(EmailsListItem)).toHaveLength(3)
    })

    it('should select correct EmailListItem', () => {
        const wrapper = shallow(<EmailsList/>)
        expect(wrapper.find(EmailsListItem).at(0).prop('selected')).toBe(true)
        expect(wrapper.find(EmailsListItem).at(1).prop('selected')).toBe(false)
        expect(wrapper.find(EmailsListItem).at(2).prop('selected')).toBe(false)
    })

    it('should dispatch selectEmail action', () => {
        const wrapper = shallow(<EmailsList/>)
        const secondItem = wrapper.find(EmailsListItem).at(1)
        secondItem.props().clickHandler('1')()
        expect(mockDispatch).toHaveBeenCalledWith(Actions.selectEmail('1'))
    })
})