import * as React from "react"
import { shallow } from "enzyme"
import { EmailsListItem } from "./EmailListItem"
import * as Styles from "./styles"
import { emailMock } from 'dev'

describe('EmailsListItem tests', () => {
    it('should properly render EmailsListItem component', () => {
        const mockClickHandler = jest.fn()
        const wrapper = shallow(<EmailsListItem
            rowId={1}
            key={emailMock._id}
            clickHandler={mockClickHandler}
            email={emailMock}
            selected={true}
        />)
        
        expect(wrapper.find(Styles.Wrapper)).toBeDefined()
        expect(wrapper.find(Styles.Wrapper).prop('selected')).toBe(true)
        expect(wrapper.find(Styles.Title)).toBeDefined()
        expect(wrapper.find(Styles.Title).text()).toBe(emailMock.title)
        expect(wrapper.find(Styles.Row)).toBeDefined()
        expect(wrapper.find(Styles.Row).text()).toBe('1')
        expect(wrapper.find(Styles.Recipients)).toBeDefined()
        expect(wrapper.find(Styles.Recipients).text()).toBe(emailMock.recipients.join(','))
    })

    it('should properly call clickHandler on click', () => {
        const mockClickHandler = jest.fn()
        const wrapper = shallow(<EmailsListItem
            rowId={1}
            key={emailMock._id}
            clickHandler={mockClickHandler}
            email={emailMock}
            selected={false}
        />)

        wrapper.find(Styles.Wrapper).simulate('click')
        expect(mockClickHandler).toHaveBeenCalledWith(emailMock._id)
    })
})