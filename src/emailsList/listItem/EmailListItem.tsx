import React from 'react'
import * as Styles from './styles'
import { Email } from "../duck"

type Props = {
    email: Email
    rowId: number
    clickHandler: (_id: string) => () => void
    selected: boolean
}

export const EmailsListItem: React.FC<Props> = ({ email, rowId, clickHandler, selected }) => {

    return (
        <Styles.Wrapper onClick={clickHandler(email._id)} selected={selected}>
            <Styles.Row>{rowId}</Styles.Row>
            <Styles.Title>{email.title}</Styles.Title>
            <Styles.Recipients>{email.recipients.join(',')}</Styles.Recipients>
        </Styles.Wrapper>
    )

}

export default EmailsListItem