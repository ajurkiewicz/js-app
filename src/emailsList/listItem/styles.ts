import styled from 'styled-components'

type Props = {
    selected: boolean
}

export const Wrapper = styled.div<Props>`
    height: 50px;
    margin: 0px 10px 0px 0px;
    display: flex;
    justify-content: space-around;
    align-items: center;
    background-color: ${ props => props.selected ? 'lightgray' : 'white'};
    cursor: pointer;
`

export const Title = styled.div`
    font-size: 12px;
    font-weight: bold;
`

export const Row = styled.div`
    font-size: 10px;
`

export const Recipients = styled.div`
    font-size: 12px;
`