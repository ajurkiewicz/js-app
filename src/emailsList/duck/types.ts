import { Action } from "redux"
import { PaginationLinks } from "pagination"

export enum ActionTypes  {
    GET_EMAILS_REQUEST = '@emailsActions/GET_EMAILS_REQUEST',
    GET_EMAILS_RESPONSE = '@emailsActions/GET_EMAILS_RESPONSE',
    SELECT_EMAIL = '@emailsActions/SELECT_EMAIL'
}

export enum EmailStatus {
    PENDING= 'pending',
    SENT = 'sent'
}

export enum EmailPriority {
    LOW = 'low',
    MEDIUM = 'medium',
    HIGH = 'high'
}

export type Email = {
    _id: string,
    title: string,
    body: string,
    sender: string,
    recipients: string[],
    priority: EmailPriority,
    status: EmailStatus
}


export type EmailsListState = {
    emails: Email[]
    totalEmailsCount: number
    emailsPerPage: number
    selectedEmail?: Email
}

export type GetEmailsResponse = {
    status: number,
    data: {
        totalEmailsCount: number,
        emailsPerPage: number,
        emails: Email[]
    },
    links: PaginationLinks
}

export interface GetEmailsRequestAction extends Action {
    pageId: number
    limit: number
}

export interface GetEmailsResponseAction extends Action {
    response: GetEmailsResponse
}

export interface SelectEmailAction extends Action {
    _id: string
}