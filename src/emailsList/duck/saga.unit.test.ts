import { ActionTypes, GetEmailsRequestAction } from "./types"
import { onChangePaginationPage, onGetEmailsRequest } from "./saga"
import { call } from "redux-saga/effects"
import { getEmails, getEmailsUsingPaginationLink } from "services"
import { ChangePaginationPageAction } from "pagination"

describe('EmailsList saga test', () => {

    it('should take GetEmailsRequestAction and yield call', () => {
        
        const mockAction: GetEmailsRequestAction = {
            type: ActionTypes.GET_EMAILS_REQUEST,
            pageId: 1,
            limit: 10
        }

        const result = onGetEmailsRequest(mockAction)

        const expectedCall = call(getEmails, 1, 10)
        expect(result.next().value).toEqual(expectedCall)
    })

    it('should take ChangePaginationPageAction and yield call', () => {
        const link: string = 'http://localhost:8080/api/emails?page=1&limit=10'
        const mockAction: ChangePaginationPageAction = {
            type: ActionTypes.GET_EMAILS_REQUEST,
            link
        }

        const result = onChangePaginationPage(mockAction)

        const expectedCall = call(getEmailsUsingPaginationLink, link)
        expect(result.next().value).toEqual(expectedCall)
    })
})