import {
    ActionTypes,
    GetEmailsRequestAction,
    GetEmailsResponse,
    GetEmailsResponseAction, SelectEmailAction
} from './types'

export const getEmailsRequest = (pageId: number, limit: number):GetEmailsRequestAction => ({
    type: ActionTypes.GET_EMAILS_REQUEST,
    pageId,
    limit
})

export const getEmailsResponse = (response: GetEmailsResponse):GetEmailsResponseAction => ({
    type: ActionTypes.GET_EMAILS_RESPONSE,
    response
})

export const selectEmail = (_id: string): SelectEmailAction => ({
    type: ActionTypes.SELECT_EMAIL,
    _id
})