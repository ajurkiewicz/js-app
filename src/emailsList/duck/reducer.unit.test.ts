import {
    ActionTypes,
    EmailsListReducer,
    EmailsListState,
    GetEmailsResponseAction,
    SelectEmailAction
} from 'emailsList'
import { emailMock } from 'dev'

const INIT_STATE : EmailsListState = {
    emails: [ { ...emailMock } ],
    totalEmailsCount: 0,
    emailsPerPage: 0,
    selectedEmail: undefined
}

describe(`Emails list reducer unit tests`, () => {
    it(`Should handle ${ActionTypes.GET_EMAILS_RESPONSE}`, () => {
        const mockedAction: GetEmailsResponseAction = {
            type: ActionTypes.GET_EMAILS_RESPONSE,
            response: {
                status: 200,
                data: {
                    emails: [{ ...emailMock }, { ...emailMock }],
                    emailsPerPage: 10,
                    totalEmailsCount: 100
                },
                links: {}
            }
        }

        expect(EmailsListReducer(INIT_STATE, mockedAction)).toEqual({
            ...INIT_STATE,
            emails: [{ ...emailMock }, { ...emailMock }],
            emailsPerPage: 10,
            totalEmailsCount: 100
        })
    })

    it(`Should handle ${ActionTypes.SELECT_EMAIL} with correct _id`, () => {
        const mockedAction: SelectEmailAction = {
            type: ActionTypes.SELECT_EMAIL,
            _id: emailMock._id
        }

        expect(EmailsListReducer(INIT_STATE, mockedAction)).toEqual({
            ...INIT_STATE,
            selectedEmail: INIT_STATE.emails[0]
        })
    })

    it(`Should handle ${ActionTypes.SELECT_EMAIL} with incorrect _id`, () => {
        const mockedAction: SelectEmailAction = {
            type: ActionTypes.SELECT_EMAIL,
            _id: ''
        }

        const INIT_STATE_SELECTED_EMAIL = {
            ...INIT_STATE,
            selectedEmail: emailMock
        }

        expect(EmailsListReducer(INIT_STATE_SELECTED_EMAIL, mockedAction)).toEqual({
            ...INIT_STATE_SELECTED_EMAIL,
            selectedEmail: undefined
        })
    })
})