import { call, put, takeLatest } from 'redux-saga/effects'
import { ActionTypes, GetEmailsRequestAction, GetEmailsResponse } from "./types"
import { ChangePaginationPageAction, ActionTypes as PaginationActionTypes } from "pagination"
import { getEmails, getEmailsUsingPaginationLink } from 'services'
import { AxiosResponse } from "axios"
import { Actions } from "./index"

export function* onGetEmailsRequest({ pageId, limit } : GetEmailsRequestAction): Generator {
    try {
        const response: AxiosResponse = (yield call(getEmails, pageId, limit)) as AxiosResponse
        yield put(Actions.getEmailsResponse(processGetEmailsResponse(response)))
    } catch (error) {

    }
}

export function* onChangePaginationPage({ link } : ChangePaginationPageAction):Generator {
    try {
        const response: AxiosResponse = (yield call(getEmailsUsingPaginationLink, link)) as AxiosResponse
        yield put(Actions.getEmailsResponse(processGetEmailsResponse(response)))
    } catch (error) {

    }
}

export const processGetEmailsResponse = (response: AxiosResponse): GetEmailsResponse => {
    return {
        status: response.status,
        ...response.data
    }
}

export const EmailsListSaga = [
    takeLatest(ActionTypes.GET_EMAILS_REQUEST, onGetEmailsRequest),
    takeLatest(PaginationActionTypes.CHANGE_PAGE, onChangePaginationPage)
]