import { createReducer } from "reduxsauce"
import {
    ActionTypes,
    EmailsListState,
    GetEmailsResponseAction, SelectEmailAction
} from "./types"

const INIT_STATE : EmailsListState = {
    emails: [],
    totalEmailsCount: 0,
    emailsPerPage: 0,
    selectedEmail: undefined
}

type Handlers = {
    [x in string]: (state: EmailsListState, action: any) => EmailsListState
}

const onGetEmailsResponse = (state: EmailsListState, { response: { data } }: GetEmailsResponseAction): EmailsListState => {
    return {
        ...state,
        emails: data.emails,
        totalEmailsCount: data.totalEmailsCount,
        emailsPerPage: data.emailsPerPage,
        selectedEmail: undefined
    }
}

const onSelectEmail = (state: EmailsListState, { _id }: SelectEmailAction): EmailsListState => {
    return {
        ...state,
        selectedEmail: state.emails.find(e => e._id === _id)
    }
}

const HANDLERS: Handlers = {
    [ActionTypes.GET_EMAILS_RESPONSE]: onGetEmailsResponse,
    [ActionTypes.SELECT_EMAIL]: onSelectEmail
}

export const EmailsListReducer = createReducer(INIT_STATE, HANDLERS)