import {
    ActionTypes,
    GetEmailsRequestAction,
    GetEmailsResponse,
    GetEmailsResponseAction,
    SelectEmailAction
} from "./types"
import * as Actions from "./actions"

describe('EmailsList actions unit tests', () => {
    it('should create correct action object using getEmailsRequest', () => {
        const expectedAction: GetEmailsRequestAction = {
            type: ActionTypes.GET_EMAILS_REQUEST,
            pageId: 1,
            limit: 10
        }
        expect(Actions.getEmailsRequest(1,10)).toEqual(expectedAction)
    })

    it('should create correct action object using getEmailsResponse', () => {
        const response: GetEmailsResponse = {
            status: 200,
            data: {
                emailsPerPage: 10,
                totalEmailsCount: 100,
                emails: []
            },
            links: {}
        }

        const expectedAction: GetEmailsResponseAction = {
            type: ActionTypes.GET_EMAILS_RESPONSE,
            response
        }

        expect(Actions.getEmailsResponse(response)).toEqual(expectedAction)
    })

    it('should create correct action object using selectEmail', () => {
        const expectedAction: SelectEmailAction = {
            type: ActionTypes.SELECT_EMAIL,
            _id: '999'
        }

        expect(Actions.selectEmail('999')).toEqual(expectedAction)
    })
})