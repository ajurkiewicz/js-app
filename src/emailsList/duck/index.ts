import * as EmailActions from './actions'
export const Actions = EmailActions
export * from './types'
export * from './reducer'
export * from './saga'