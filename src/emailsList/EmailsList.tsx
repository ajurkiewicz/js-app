import React from 'react'
import * as Styles from './styles'
import { shallowEqual, useDispatch, useSelector } from "react-redux"
import { AppState } from "config"
import { Actions, Email } from "./duck"
import { EmailsListItem } from "emailsList"

export const EmailsList: React.FC<{}> = () => {
    const emails: Email[]  = useSelector<AppState>(state => state.emailsList.emails, shallowEqual) as Email[]
    const selectedEmail = useSelector<AppState>(state => state.emailsList.selectedEmail, shallowEqual) as Email
    const selectedEmailId = selectedEmail ? selectedEmail._id : ''

    const dispatch = useDispatch()

    const onListItemClick = (_id: string) => () => {
        dispatch(Actions.selectEmail(_id))
    }
    
    return (
        <Styles.Wrapper>
            {
                emails.map((email, index) =>
                    <EmailsListItem 
                        key={email._id}
                        email={email} 
                        rowId={index+1}
                        selected={selectedEmailId === email._id}
                        clickHandler={onListItemClick}
                    />
                )
            }
        </Styles.Wrapper>
    )
}

export default EmailsList