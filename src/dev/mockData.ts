import { Email, EmailPriority, EmailStatus } from "emailsList"
import { PaginationLinks } from "pagination"

export const emailMock: Email = {
    _id: '100',
    title: 'test title',
    body: 'test body',
    sender: 'test@test.pl',
    recipients: ['test2@test.pl'],
    priority: EmailPriority.LOW,
    status: EmailStatus.PENDING
}

export const twoPaginationLinksMock: PaginationLinks = {
    first: 'http://localhost:8080/api/emails?page=1&limit=10',
    last: 'http://localhost:8080/api/emails?page=1&limit=10'
}

export const threePaginationLinksMock: PaginationLinks = {
    first: 'http://localhost:8080/api/email&page=0&limit=25',
    next: 'http://localhost:8080/api/email&page=1&limit=25',
    last: 'http://localhost:8080/api/email&page=2&limit=25'
}

export const fourPaginationLinksMock: PaginationLinks = {
    first: 'http://localhost:8080/api/email&page=0&limit=25',
    prev: 'http://localhost:8080/api/email&page=1&limit=25',
    next: 'http://localhost:8080/api/email&page=3&limit=25',
    last: 'http://localhost:8080/api/email&page=4&limit=25'
}