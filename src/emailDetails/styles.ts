import styled from 'styled-components'

export const Wrapper = styled.div`
    width: 400px;
    height: 200px;
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    align-items: center;
`