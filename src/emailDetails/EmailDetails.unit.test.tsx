import * as React from "react"
import { shallow } from "enzyme"
import { EmailDetails } from "./EmailDetails"
import * as Styles from "./styles"
import { emailMock } from "dev"

const mockSelector = jest.fn()

jest.mock('react-redux', () => ({
    useSelector: () => mockSelector(),
    useDispatch: jest.fn()
}))

describe('EmailDetails tests', () => {
    it('should properly render EmailDetails component', () => {
        mockSelector.mockReturnValue(emailMock)

        const wrapper = shallow(<EmailDetails />)
        expect(wrapper.find(Styles.Wrapper)).toBeDefined()
        expect(wrapper.find(Styles.Wrapper).children()).toHaveLength(6)
        expect(wrapper.find('div[children="test title"]')).toHaveLength(1)
        expect(wrapper.find('div[children="test body"]')).toHaveLength(1)
        expect(wrapper.find('div[children="test@test.pl"]')).toHaveLength(1)
        expect(wrapper.find('div[children="test2@test.pl"]')).toHaveLength(1)
        expect(wrapper.find('div[children="pending"]')).toHaveLength(1)
        expect(wrapper.find('div[children="low"]')).toHaveLength(1)
    })
})