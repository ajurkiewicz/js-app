import React from 'react'
import * as Styles from './styles'
import { Email } from "emailsList"
import { useSelector } from "react-redux"
import { AppState } from "../config"

export const EmailDetails: React.FC<{}> = () => {
    const email: Email = useSelector<AppState>(state => state.emailsList.selectedEmail) as Email
    return (
        email ? <Styles.Wrapper>
            <div>{email.title}</div>
            <div>{email.body}</div>
            <div>{email.sender}</div>
            <div>{email.recipients.join(",")}</div>
            <div>{email.priority}</div>
            <div>{email.status}</div>
        </Styles.Wrapper>: null
    )
}