import React, { useEffect, useState } from 'react'
import * as Styles from './styles'
import { useDispatch, useSelector } from "react-redux"
import { AppState } from "../config"
import { Actions, PaginationAction, PaginationButton, PaginationLinks } from "./duck"
import { Button } from '@material-ui/core'

const initPaginationButtons :PaginationButton[] = [
    {
        type: PaginationAction.FIRST,
        title: PaginationAction.FIRST
    },
    {
        type: PaginationAction.PREV,
        title: PaginationAction.PREV
    },
    {
        type: PaginationAction.NEXT,
        title: PaginationAction.NEXT
    },
    {
        type: PaginationAction.LAST,
        title: PaginationAction.LAST
    }
]

export const isFirstButtonAndPrevLinkIsUndefined = (button: PaginationButton, paginationLinks: PaginationLinks) =>{
    return !paginationLinks[PaginationAction.PREV] && button.type === PaginationAction.FIRST
}

export const isLastButtonAndNextLinkIsUndefined = (button: PaginationButton, paginationLinks: PaginationLinks) =>{
    return !paginationLinks[PaginationAction.NEXT] && button.type === PaginationAction.LAST
}

export const getUpdatedPaginationButtons = (paginationButtons: PaginationButton[], paginationLinks: PaginationLinks) => {
    return paginationButtons.map((button) => {
        return {
            ...button,
            link: paginationLinks[button.type]
        }
    })
}

export const Pagination: React.FC<{}> = () => {
    const dispatch = useDispatch()
    const paginationLinks: PaginationLinks = useSelector<AppState>(state => state.pagination.links) as PaginationLinks
    const [paginationButtons, setPaginationButtons] = useState<PaginationButton[]>(getUpdatedPaginationButtons(initPaginationButtons, paginationLinks))

    useEffect(() => {
        setPaginationButtons(getUpdatedPaginationButtons(initPaginationButtons, paginationLinks))
    }, [paginationLinks])


    const isButtonDisabled = (button: PaginationButton) => {
        const isFirstButtonAndShouldBeDisabled = isFirstButtonAndPrevLinkIsUndefined(button,paginationLinks)
        const isLastButtonAndShouldBeDisabled = isLastButtonAndNextLinkIsUndefined(button,paginationLinks)
        if (!button.link || isFirstButtonAndShouldBeDisabled || isLastButtonAndShouldBeDisabled) {
            return true
        }
        return false
    }
    
    const changePaginationPage = ({ link }: PaginationButton) => () => {
        if (link) {
            dispatch(Actions.changePaginationPage(link))
        }
    }

    return (
        <Styles.Wrapper>
            {
                paginationButtons.map((button) => (
                    <Button
                        key={button.type}
                        disabled={isButtonDisabled(button)}
                        onClick={changePaginationPage(button)}
                        variant='outlined'>
                        {button.title}
                    </Button>
                ))
            }
        </Styles.Wrapper >
    )
        
}