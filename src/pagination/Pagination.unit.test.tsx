import * as React from 'react'
import {
    Actions,
    PaginationAction,
    PaginationButton
} from "pagination/duck"
import {
    getUpdatedPaginationButtons,
    isFirstButtonAndPrevLinkIsUndefined,
    isLastButtonAndNextLinkIsUndefined,
    Pagination
} from "pagination/Pagination"

import { shallow } from "enzyme"
import { Button } from "@material-ui/core"
import { fourPaginationLinksMock, threePaginationLinksMock, twoPaginationLinksMock } from "../dev"

const mockSelector = jest.fn()
const mockDispatch = jest.fn()

jest.mock('react-redux', () => ({
    useSelector: () => mockSelector(),
    useDispatch: () => mockDispatch
}))

describe('Pagination tests', () => {
    let initButtons: PaginationButton[]

    beforeAll(() => {
        initButtons = [
            {
                type: PaginationAction.FIRST,
                title: 'first'
            },
            {
                type: PaginationAction.LAST,
                title: 'last'
            }
        ]
    })
    it('getUpdatedPaginationButtons should return updated buttons',() => {
        const expectedButtons: PaginationButton[] = [
            {
                type: PaginationAction.FIRST,
                title: 'first',
                link: 'http://localhost:8080/api/emails?page=1&limit=10'
            },
            {
                type: PaginationAction.LAST,
                title: 'last',
                link: 'http://localhost:8080/api/emails?page=1&limit=10'
            }
        ]
        
        expect(getUpdatedPaginationButtons(initButtons,twoPaginationLinksMock)).toEqual(expectedButtons)
    })

    it('should return correct boolean value from isFirstButtonAndPrevLinkIsUndefined',() => {
        expect(isFirstButtonAndPrevLinkIsUndefined(initButtons[0],twoPaginationLinksMock)).toBe(true)
        expect(isFirstButtonAndPrevLinkIsUndefined(initButtons[1],twoPaginationLinksMock)).toBe(false)
    })

    it('should return correct boolean value from isLastButtonAndNextLinkIsUndefined',() => {
        expect(isLastButtonAndNextLinkIsUndefined(initButtons[0],twoPaginationLinksMock)).toBe(false)
        expect(isLastButtonAndNextLinkIsUndefined(initButtons[1],twoPaginationLinksMock)).toBe(true)
    })

    it('should correctly render Pagination with all buttons disabled',() => {
        mockSelector.mockReturnValue(twoPaginationLinksMock)
        const wrapper = shallow(<Pagination />)
        const buttons = wrapper.find(Button)
        expect(buttons.find({ 'disabled': true })).toHaveLength(4)
        expect(buttons.find({ 'disabled': false })).toHaveLength(0)
    })

    it('should correctly render Pagination with 2 buttons disabled',() => {
        mockSelector.mockReturnValue(threePaginationLinksMock)
        const wrapper = shallow(<Pagination />)
        const buttons = wrapper.find(Button)
        expect(buttons.find({ 'disabled': true })).toHaveLength(2)
        expect(buttons.find({ 'disabled': false })).toHaveLength(2)
    })

    it('should correctly render Pagination with all buttons enabled',() => {
        mockSelector.mockReturnValue(fourPaginationLinksMock)
        
        const wrapper = shallow(<Pagination />)
        const buttons = wrapper.find(Button)
        expect(buttons.find({ 'disabled': true })).toHaveLength(0)
        expect(buttons.find({ 'disabled': false })).toHaveLength(4)
    })

    it('should call dispatch on enabled button click',() => {
        mockSelector.mockReturnValue(fourPaginationLinksMock)
        const wrapper = shallow(<Pagination />)
        const firstButton = wrapper.find(Button).at(0)
        firstButton.simulate('click')

        expect(mockDispatch).toHaveBeenCalledWith(Actions.changePaginationPage('http://localhost:8080/api/email&page=0&limit=25'))
    })

})