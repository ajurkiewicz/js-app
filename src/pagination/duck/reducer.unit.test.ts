import { ActionTypes as EmailsListActionTypes, GetEmailsResponseAction } from 'emailsList'
import { PaginationReducer } from './reducer'
import { PaginationState } from "./types"

const INIT_STATE : PaginationState = {
    links: {},
    currentPage: 0
}

describe(`Pagination reducer unit test`, () => {

    it(`Should handle ${EmailsListActionTypes.GET_EMAILS_RESPONSE}`, () => {
        const mockedLinks = {
            first: 'http://localhost:8080/api/emails?page=1&limit=10',
            next: 'http://localhost:8080/api/emails?page=2&limit=10',
            last: 'http://localhost:8080/api/emails?page=10&limit=10'
        }

        const mockAction: GetEmailsResponseAction = {
            type: EmailsListActionTypes.GET_EMAILS_RESPONSE,
            response: {
                status: 200,
                data: {
                    emails: [],
                    emailsPerPage: 10,
                    totalEmailsCount: 100
                },
                links: mockedLinks
            }
        }

        expect(PaginationReducer(INIT_STATE, mockAction)).toEqual({
            ...INIT_STATE,
            links: mockedLinks
        })
    })
})