import * as PaginationAction from './actions'
export const Actions = PaginationAction
export * from './types'
export * from './reducer'