import { Action } from "redux"

export enum ActionTypes {
    CHANGE_PAGE = '@pagination/CHANGE_PAGE'
}

export type PaginationLinks = {
    first?: string
    last?: string
    prev?: string
    next?: string
}

export type PaginationButton = {
    type: PaginationAction
    title: string
    link?: string
}

export enum PaginationAction {
    FIRST = 'first',
    LAST = 'last',
    NEXT = 'next',
    PREV = 'prev'
}

export type PaginationState = {
    links: PaginationLinks,
    currentPage: number
}

export interface ChangePaginationPageAction extends Action {
    link: string
}
