import { ActionTypes, ChangePaginationPageAction } from "./types"

export const changePaginationPage = (link: string) : ChangePaginationPageAction => ({
    type: ActionTypes.CHANGE_PAGE,
    link
})