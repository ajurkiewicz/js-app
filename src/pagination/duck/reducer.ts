import { PaginationState } from "./types"
import { ActionTypes as EmailsListActionTypes, GetEmailsResponseAction } from 'emailsList/duck'
import { createReducer } from "reduxsauce"

const INIT_STATE : PaginationState = {
    links: {},
    currentPage: 0
}

const onGetEmailsResponse = (state: PaginationState,{ response: { links } }: GetEmailsResponseAction): PaginationState => ({
    ...state,
    links
})

type Handlers = {
    [x in string]: (state: PaginationState, action: any) => PaginationState
}

const HANDLERS : Handlers = {
    [EmailsListActionTypes.GET_EMAILS_RESPONSE]: onGetEmailsResponse
}

export const PaginationReducer = createReducer(INIT_STATE, HANDLERS)