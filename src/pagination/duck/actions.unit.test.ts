import { ActionTypes, ChangePaginationPageAction } from "./types"
import * as Actions from "./actions"

describe('Pagination actions unit tests', () => {
    it('should create correct action object using changePaginationPage', () => {
        const expectedAction: ChangePaginationPageAction = {
            type: ActionTypes.CHANGE_PAGE,
            link: 'test link'
        }
        expect(Actions.changePaginationPage('test link')).toEqual(expectedAction)
    })
})