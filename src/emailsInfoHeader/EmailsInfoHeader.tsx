import React from "react"
import * as Styles from './styles'
import { useSelector } from "react-redux"
import { AppState } from "config"
import { EmailsInfo } from "./duck"

export const EmailsInfoHeader: React.FC<{}> = () => {
    const emailsInfo: EmailsInfo = useSelector<AppState>(state => ({
        totalEmailsCount: state.emailsList.totalEmailsCount,
        emailsPerPage: state.emailsList.emailsPerPage
    })) as EmailsInfo

    return (
        <Styles.Wrapper>
            <div>Number of emails : {emailsInfo.totalEmailsCount}</div>
            <div>Emails per page: {emailsInfo.emailsPerPage}</div>
        </Styles.Wrapper>
    )
}