import styled from "styled-components"

export const Wrapper = styled.div`
    width: 100%;
    height: 40px;
    display: flex;
    justify-content: space-evenly;
    align-items: center;
`