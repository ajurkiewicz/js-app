export type EmailsInfo = {
    totalEmailsCount: number,
    emailsPerPage: number
}