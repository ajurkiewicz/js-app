import * as React from "react"
import { shallow } from "enzyme"
import { EmailsInfoHeader } from "./EmailsInfoHeader"
import * as Styles from "./styles"

jest.mock('react-redux', () => ({
    useSelector: () => ({
        totalEmailsCount: 123,
        emailsPerPage: 10
    }),
    useDispatch: jest.fn()
}))

describe('EmailsInfoHeader tests', () => {
    it('should properly render EmailsInfoHeader component', () => {
        const wrapper = shallow(<EmailsInfoHeader />)
        expect(wrapper.find(Styles.Wrapper)).toBeDefined()
        expect(wrapper.find('div[children="Number of emails : 123"]')).toBeDefined()
        expect(wrapper.find('div[children="Emails per page : 10"]')).toBeDefined()
    })
})