import axios from "axios"
import { getEmails, getEmailsUsingPaginationLink } from './api'
import CONFIG from "app/config"
const { apiHost, apiBaseUri } = CONFIG

jest.mock('axios')

describe('Services tests', () => {
    let axiosGetSpy: any

    beforeAll(()=> {
        axiosGetSpy = jest.spyOn(axios, "get")
    })

    it('should call axios get with correct parameters for getEmails', () => {
        getEmails(1,10)
        expect(axiosGetSpy).toHaveBeenCalledWith((`${apiHost}${apiBaseUri}?page=1&limit=10`))
    })

    it('should call axios get with correct parameters for getEmailsUsingPaginationLink', () => {
        getEmailsUsingPaginationLink(`${apiHost}${apiBaseUri}?page=1&limit=10`)
        expect(axiosGetSpy).toHaveBeenCalledWith((`${apiHost}${apiBaseUri}?page=1&limit=10`))
    })
})