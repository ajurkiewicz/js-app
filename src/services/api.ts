import axios, { AxiosResponse } from 'axios'
import CONFIG from 'app/config'
const { apiHost, apiBaseUri } = CONFIG

export const getEmails = (pageId: number, limit: number): Promise<AxiosResponse> => {
    return axios.get(`${apiHost}${apiBaseUri}?page=${pageId}&limit=${limit}`)
}

export const getEmailsUsingPaginationLink = (link: string): Promise<AxiosResponse> => {
    return axios.get(link)
}