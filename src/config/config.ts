import { applyMiddleware, createStore } from "redux"
import { appReducer } from './rootReducer'
import { rootSagas, sagaMiddleware } from "./rootSagas"

const middleWares = [sagaMiddleware]
export const store = createStore(appReducer, applyMiddleware(...middleWares))

sagaMiddleware.run(rootSagas)