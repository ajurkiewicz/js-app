import { EmailsListState } from 'emailsList'
import { PaginationState } from 'pagination'

export type AppState = {
    emailsList: EmailsListState,
    pagination: PaginationState
}