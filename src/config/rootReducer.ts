import { combineReducers, Reducer } from "redux"
import { AppState } from "./types"
import { EmailsListReducer } from "emailsList"
import { PaginationReducer } from "../pagination/duck"

export const appReducer: Reducer<AppState> = combineReducers<AppState>({
    emailsList: EmailsListReducer,
    pagination: PaginationReducer
})