import createSagaMiddleware from 'redux-saga'
import { all } from 'redux-saga/effects'
import { EmailsListSaga } from "emailsList"

export function* rootSagas() {
    yield all(
        [
            ...EmailsListSaga
        ]
    )
}

export const sagaMiddleware = createSagaMiddleware()
