import { createStore } from "redux"
import { appReducer } from './rootReducer'
import { ChangePaginationPageAction, ActionTypes as PaginationActionTypes, PaginationReducer } from "pagination"
import { ActionTypes, EmailsListReducer, GetEmailsResponseAction } from "emailsList"

const store = createStore(appReducer)

describe('App reducer tests', () => {
    it('App reducer should contain correct reducers', () => {
        expect(store.getState().emailsList).toEqual(EmailsListReducer(undefined, { type: '' }))
        expect(store.getState().pagination).toEqual(PaginationReducer(undefined, { type: '' }))
    })

    it('should handle GetEmailsResponseAction action', () => {
        const getEmailsResponseMockAction: GetEmailsResponseAction = {
            type: ActionTypes.GET_EMAILS_RESPONSE,
            response: {
                status: 200,
                data: {
                    emails: [],
                    emailsPerPage: 25,
                    totalEmailsCount: 100
                },
                links: {}
            }
        }

        store.dispatch(getEmailsResponseMockAction)
        expect(store.getState().emailsList).toEqual(EmailsListReducer(undefined, getEmailsResponseMockAction))
    })

    it('should handle ChangePaginationPageAction action', () => {
        const changePaginationPageAction: ChangePaginationPageAction = {
            type: PaginationActionTypes.CHANGE_PAGE,
            link: 'http://localhost:8080/api/emails&page=1&limit=10'
        }

        store.dispatch(changePaginationPageAction)
        expect(store.getState().pagination).toEqual(PaginationReducer(undefined, changePaginationPageAction))
    })
})