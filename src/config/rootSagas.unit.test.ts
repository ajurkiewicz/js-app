import { rootSagas } from './rootSagas'
import { all } from 'redux-saga/effects'
import { EmailsListSaga } from 'emailsList'

describe('Root sagas unit tests', () => {
    it('should yield all sagas', () => {
        const result = rootSagas()

        const expected = all(
            [
                ...EmailsListSaga
            ]
        )

        expect(result.next().value).toEqual(expected)
    })
})