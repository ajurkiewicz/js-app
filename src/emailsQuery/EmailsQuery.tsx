import React, { useState } from 'react'
import { Button, TextField } from '@material-ui/core'
import * as Styles from './styles'
import { useDispatch } from "react-redux"
import { Actions } from 'emailsList'
import CONFIG from 'app/config'

export const EmailsQuery: React.FC<{}> = () => {
    const [pageId,setPageId] = useState<string>(CONFIG.initPage.toString())
    const [limit,setLimit] = useState<string>(CONFIG.limit.toString())
    const dispatch = useDispatch()

    const fetchEmails = () => {
        dispatch(Actions.getEmailsRequest(parseInt(pageId), parseInt(limit)))
    }

    const onPageIdChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setPageId(event.target.value)
    }

    const onLimitChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setLimit(event.target.value)
    }

    return (
        <Styles.Wrapper>
            <TextField type='number' id='pageId' label='Page id' value={pageId} onChange={onPageIdChange}/>
            <TextField type='number' id='limit' label='Query Limit'  value={limit} onChange={onLimitChange} />
            <Button title='Fetch' variant='outlined' onClick={fetchEmails}>Fetch Emails</Button>
        </Styles.Wrapper>
    )
}