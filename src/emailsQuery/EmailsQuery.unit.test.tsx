import * as React from "react"
import { shallow, ShallowWrapper } from "enzyme"
import { EmailsQuery } from "./EmailsQuery"
import * as Styles from "./styles"
import { Button, TextField } from "@material-ui/core"
import { ActionTypes } from "emailsList"

const mockDispatch = jest.fn()

jest.mock('react-redux', () => ({
    useSelector: jest.fn(),
    useDispatch: () => mockDispatch
}))

describe('EmailsQuery tests', () => {

    const getTextFieldById = (wrapper: ShallowWrapper, id: string) => {
        return wrapper.find(TextField).find({ id })
    }

    it('should properly render EmailsQuery component', () => {
        const wrapper = shallow(<EmailsQuery />)
        expect(wrapper.find(Styles.Wrapper)).toBeDefined()
        expect(wrapper.find(TextField)).toHaveLength(2)
        expect(wrapper.find(Button)).toHaveLength(1)
        expect(wrapper.find(Button).text()).toBe('Fetch Emails')
    })

    it('should properly update page id TextField', () => {
        const wrapper = shallow(<EmailsQuery />)
        const pageIdTextField = getTextFieldById(wrapper,'pageId')

        expect(pageIdTextField).toHaveLength(1)
        expect(pageIdTextField.prop('value')).toBe("1")

        pageIdTextField.simulate('change', {
            target: {
                value: "3"
            }
        })

        expect(getTextFieldById(wrapper, 'pageId').prop('value')).toBe("3")
    })

    it('should properly update limit TextField', () => {
        const wrapper = shallow(<EmailsQuery />)
        const limitTextField = getTextFieldById(wrapper,'limit')

        expect(limitTextField).toHaveLength(1)
        expect(limitTextField.prop('value')).toBe("25")

        limitTextField.simulate('change', {
            target: {
                value: "50"
            }
        })

        expect(getTextFieldById(wrapper, 'limit').prop('value')).toBe("50")
    })

    it('should dispatch with current parameters', () => {
        const dispatchParams = {
            type: ActionTypes.GET_EMAILS_REQUEST,
            pageId: 1,
            limit: 25
        }
        const wrapper = shallow(<EmailsQuery />)
        const fetchButton = wrapper.find(Button)

        fetchButton.simulate('click', {})
        expect(mockDispatch).toHaveBeenCalledWith(dispatchParams)
    })
})