import React, { useEffect } from 'react'
import * as Styles from './styles'
import { EmailsList } from 'emailsList'
import { EmailDetails } from "emailDetails"
import { useDispatch } from 'react-redux'
import { Actions } from 'emailsList'
import { EmailsQuery } from 'emailsQuery'
import { Pagination } from 'pagination'
import CONFIG from './config'
import { EmailsInfoHeader } from 'emailsInfoHeader'

export const App: React.FC<{}> = () => {
    const dispatch = useDispatch()

    useEffect(() => {
        const getEmailsOnInit = () =>{
            dispatch(Actions.getEmailsRequest(CONFIG.initPage,CONFIG.limit))
        }
        getEmailsOnInit()
    }, [dispatch])
    
    return (
        <Styles.Wrapper>
            <Styles.RowWrapper>
                <EmailsInfoHeader />
            </Styles.RowWrapper>
            <Styles.RowWrapper>
                <Styles.EmailsListWrapper>
                    <EmailsQuery/>
                    <EmailsList/>
                    <Pagination />
                </Styles.EmailsListWrapper>
                <Styles.EmailsDetailsWrapper>
                    <EmailDetails/>
                </Styles.EmailsDetailsWrapper>
            </Styles.RowWrapper>
        </Styles.Wrapper>
    )
}

export default App