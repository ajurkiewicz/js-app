import * as React from 'react'
import { shallow } from 'enzyme'
import { App } from './App'
import * as Styles from './styles'
import { ActionTypes, GetEmailsRequestAction } from "emailsList"

const mockDispatch = jest.fn()

jest.mock('react-redux', () => ({
    useSelector: () => jest.fn(),
    useDispatch: () => mockDispatch
}))

describe('App tests', () => {
    let useEffectSpy: any

    beforeAll(() => {
        useEffectSpy = jest.spyOn(React, "useEffect").mockImplementation(f => f())
    })

    it('should properly render App component', () => {
        const app = shallow(<App/>)
        expect(app.find(Styles.Wrapper)).toBeDefined()
        expect(app.find(Styles.RowWrapper)).toBeDefined()
        expect(app.find(Styles.EmailsDetailsWrapper)).toBeDefined()
        expect(app.find(Styles.EmailsListWrapper)).toBeDefined()

        expect(app.find(Styles.RowWrapper)).toHaveLength(2)
        expect(app.find(Styles.EmailsListWrapper).children()).toHaveLength(3)
        expect(app.find(Styles.EmailsDetailsWrapper).children()).toHaveLength(1)
    })

    it('should properly call useEffect and dispatch', () => {
        const getEmailsRequestAction: GetEmailsRequestAction = {
            type: ActionTypes.GET_EMAILS_REQUEST,
            pageId: 1,
            limit: 25
        }

        shallow(<App />)
        expect(useEffectSpy).toHaveBeenCalled()
        expect(mockDispatch).toHaveBeenCalledWith(getEmailsRequestAction)
    })
})