export default {
    initPage: 1,
    limit: 25,
    apiHost: 'http://localhost:8080/',
    apiBaseUri: 'api/emails'
}