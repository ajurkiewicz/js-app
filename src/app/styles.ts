import styled from 'styled-components'

export const Wrapper = styled.div`
    display: flex;
    width: 1000px;
    height: 650px;
    flex-direction: column;
    justify-content: space-around;
    align-content: space-around;
    border: 1px solid black;
    margin: 0 auto;
    padding: 10px;
`

export const RowWrapper = styled.div`
    display: flex;
    min-height: 50px;
`

export const EmailsListWrapper = styled.div`
    width: 700px;
    height: 550px;
    display: flex;
    justify-content: space-around;
    flex-direction: column;
`

export const EmailsDetailsWrapper = styled.div`
    display: flex;
    align-items: center;
    width: 300px;
    height: 100%;
`