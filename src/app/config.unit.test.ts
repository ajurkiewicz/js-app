import CONFIG from './config'

describe('App config unit tests', () => {
    it('should return correct config parameters', () => {
        expect(CONFIG.apiHost).toBe("http://localhost:8080/")
        expect(CONFIG.apiBaseUri).toBe("api/emails")
        expect(CONFIG.limit).toBe(25)
        expect(CONFIG.initPage).toBe(1)
    })
})